# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import os
import re
import shutil
import sys
import threading
import time
import zipfile
from decimal import Decimal
from pathlib import Path
import translators as ts

threadList = []
tempFileDirList = []

FROM_LANG = "en"
TO_LANG = "zh"
ENCODING = "utf-8"
cacheFilePath = "缓存文件.txt"


class ProgressBar:
    """
    进度条对象
    """
    maxValue = 100
    title = "进度"
    value = 0
    id = 1
    finishFlag = False
    priority = 0

    def __init__(self, title: str, maxValue: int, id: int):
        """
        初始化进度条对象
        :param title: 进度条标题
        :param maxValue: 进度条最大值
        :param id: 进度条的Id, 必须唯一否则会出现错误
        """
        self.maxValue = maxValue
        self.title = title
        self.id = id

    def setValue(self, value: int):
        """
        设置进度值
        :param value:
        :return:
        """
        if self.finishFlag:
            return
        self.value = value

    def finish(self):
        self.finishFlag = True

    def setPriority(self, priority: int):
        """
        设置优先级, 越小越优先
        :param priority: 优先值
        :return:
        """
        self.priority = priority


class ProgressBarPool:
    """
    进度条池
    """
    pool: list[ProgressBar] = []

    def __init__(self):
        pass

    def add(self, progressBar: ProgressBar):
        """
        添加一个进度条
        :param progressBar:
        :return:
        """
        for i in self.pool:
            if i.id == progressBar.id:
                raise Exception("id not unique, new ProgressBar Id :" + str(progressBar.id))

        self.pool.append(progressBar)
        self.pool.sort(key=lambda i: i.priority)

    def run(self):
        """
        运行进度条
        :return:
        """
        t = threading.Thread(target=self.printProgress, args=(pool,))
        threadList.append(t)
        t.start()

    @staticmethod
    def printProgress(poolElement):
        count = 0
        while count != len(poolElement.pool):
            count = 0
            s = "-" * 30 + "\n"
            for i in poolElement.pool:
                if not i.finishFlag:
                    s += i.title + ": {}%: ".format(
                        Decimal(i.value / i.maxValue * 100).quantize(Decimal('0.00'))) + "▓" * (
                                 int(i.value / i.maxValue * 100) // 2) + "\n"
                else:
                    count += 1
            print(s + "-" * 30, flush=True)
            time.sleep(0.7)

    def setProgressBarValue(self, id: int, value: int):
        """
        设置指定进度条的
        :param id: 进度条的Id
        :param value: 要设置的进度条的进度值
        :return:
        """
        for i in self.pool:
            if i.id == id and not i.finishFlag:
                i.setValue(value)
                if value == i.maxValue:
                    self.finishProgressBar(id)

    def setProgressBarMaxValue(self, id: int, maxValue: int):
        """
        设置指定Id的进度条的最大值
        :param id: 进度条的Id
        :param maxValue: 要设置的最大值
        :return:
        """
        for i in self.pool:
            if i.id == id:
                i.maxValue = maxValue

    def finishProgressBar(self, id: int):
        """
        设置指定Id的进度条为完成状态
        :param id: 进度条的Id
        :return:
        """
        for i in self.pool:
            if i.id == id:
                i.finishFlag = True


# 进度条池
pool = ProgressBarPool()
# 同步锁
look = threading.Lock()
# 翻译数据缓存
translateData = {}
if not os.path.exists(cacheFilePath):
    with open(cacheFilePath, "w") as f:
        f.write("{}")
with open(cacheFilePath, "r", encoding="utf-8") as f:
    for i in f.readlines():
        sp = i.split(":")
        key = sp[0].replace("\n", "")[1:-1]
        value = sp[1].replace("\n", "")[1:-1]
        translateData[key] = value


def translate(text: str) -> str:
    """
    翻译英文为中文
    :param text:
    :return:
    """

    try:
        # 先去缓存里面找, 如果没有在调用Api
        rs = translateData.get(text)
        if rs is None:
            look.acquire()
            rs = ts.google(text, 'en', 'zh-CN')

            # 保存入缓存中
            translateData[text] = rs
            # 写入缓存文件
            with open(cacheFilePath, "a", encoding="utf-8") as outFile:
                outFile.write("\"" + text + "\":\"" + rs + "\"\n")
            look.release()
        return rs
    except Exception as e:
        print("翻译Api查询失败")
        return translate(text)


def findFile(fileName: str, rootPath: str) -> list[str]:
    """
    此函数的可以使用正则表达式查找所有文件(包含子目录)
    :param fileName: 文件名
    :param rootPath: 根目录
    :return:
    """
    p = Path(rootPath)
    r = p.rglob(fileName)
    rs = []
    for i in r:
        path = str(i.absolute())
        if not os.path.isdir(path) and not path.endswith("data.txt"):
            rs.append(path)
    return rs


def un_zip(file_name) -> str:
    """
    解压zip压缩格式的文件
    :param file_name:
    :return:
    """
    if os.path.exists(file_name + "[temp]"):
        return file_name + "[temp]"
    zip_file = zipfile.ZipFile(file_name)
    if os.path.isdir(file_name + "[temp]"):
        pass
    else:
        os.mkdir(file_name + "[temp]")
    for names in zip_file.namelist():
        zip_file.extract(names, file_name + "[temp]")
    zip_file.close()
    return file_name + "[temp]"


def translateForJsonFile(filePath: str, outPath: str, id: int):
    """
    翻译json文件
    :param id: 进度条的Id
    :param filePath: 需要翻译的json文件地址
    :param outPath: 输出的json文件地址
    :return:
    """
    # 如果已经存在了翻译则不翻译
    if os.path.exists(outPath) or os.path.exists(filePath.replace("en_us.json", "zh_cn.json")):
        pool.finishProgressBar(id)
        return
    tempOutPath = outPath + "[undone]"

    with open(filePath, "r", encoding='utf-8') as readFile:
        with open(tempOutPath, "w", encoding='utf-8') as outFile:
            # 调整进度条最大值
            lines = readFile.readlines()
            maxValue = len(lines)
            pool.setProgressBarMaxValue(id, maxValue)
            # 调整进度条初始值
            progress = 0
            pool.setProgressBarValue(id, progress)
            for lineText in lines:
                rs = re.search("\".*\":[ ]+\"(?P<en>.*)\"", lineText)
                if rs is None:
                    outFile.write(lineText)
                else:
                    text1 = rs.group("en")
                    tText = translate(text1)

                    outFile.write(lineText.replace(text1, tText))
                    outFile.flush()
                progress += 1
                pool.setProgressBarValue(id, progress)
    # 翻译完成
    os.rename(tempOutPath, tempOutPath.replace("[undone]", ""))


def zipCompress(dirpath):
    """
    压缩文件
    :param dirpath: 要压缩的文件的路径
    :return:
    """
    output_name = f"{dirpath}.zip"
    parent_name = os.path.dirname(dirpath)
    zip = zipfile.ZipFile(output_name, "w", zipfile.ZIP_DEFLATED)
    # 多层级压缩
    for root, dirs, files in os.walk(dirpath):
        for file in files:
            if str(file).startswith("~$"):
                continue
            filepath = os.path.join(root, file)
            writepath = os.path.relpath(filepath, parent_name)
            zip.write(filepath, writepath.replace("Nages自动翻译资源包\\", ""))
    zip.close()


def deleteDir(dir):
    """
    删除文件
    :param dir: 要删除的目录
    :return:
    """
    if not os.path.exists(dir):
        return False
    if os.path.isfile(dir):
        os.remove(dir)
        return
    for i in os.listdir(dir):
        t = os.path.join(dir, i)
        if os.path.isdir(t):
            deleteDir(t)  # 重新调用次方法
        else:
            os.unlink(t)
    os.rmdir(dir)  # 递归删除目录下面的空文件夹


def app_path() -> str:
    """
    获取资源目录
    :return:
    """
    # # Returns the base application path.
    # if hasattr(sys, 'frozen'):
    #     # Handles PyInstaller
    #     print("exe目录")
    #     return os.path.dirname(os.path.realpath(sys.executable))  # 使用pyinstaller打包后的exe目录
    # return os.path.dirname(__file__)
    if getattr(sys, 'frozen', False):  # 是否Bundle Resource
        base_path = sys._MEIPASS
        return base_path
    else:
        base_path = os.path.abspath(".")
        return base_path


print("正在进行准备工作....")

# 判断输出文件是否存在
if not os.path.exists("./Nages自动翻译资源包"):
    os.makedirs("./Nages自动翻译资源包")
# 复制图片
shutil.copy(app_path() + '\\res\\pack.png', './Nages自动翻译资源包\\pack.png')
# 复制mata文件
shutil.copy(app_path() + '\\res\\pack.mcmeta', './Nages自动翻译资源包\\pack.mcmeta')

jsonPathList: list[dict] = []
count = 1
for jarFileName in findFile("*.jar", "."):
    tempFileDir = un_zip(jarFileName)
    tempFileDirList.append(tempFileDir)
    filePathList = findFile("en_us.json", tempFileDir)
    for i in filePathList:
        jsonPathList.append({"jsonPath": i, "tempFileDir": tempFileDir})

for i in jsonPathList:
    path = i.get("jsonPath").replace("en_us.json", "zh_cn.json").replace(i.get("tempFileDir") + "\\",
                                                                         os.getcwd() + "\\Nages自动翻译资源包\\")
    ddir = path[:path.rfind("\\")]
    if not os.path.exists(ddir):
        os.makedirs(ddir)
    progressBar = ProgressBar(path.replace(tempFileDir, ""), 100, count)
    pool.add(progressBar)
    t = threading.Thread(target=translateForJsonFile, args=(i.get("jsonPath"), path, count,))
    threadList.append(t)
    t.start()
    count += 1
print("准备结束开始翻译")
pool.run()
time.sleep(5)
for t in threadList:
    t.join()

print("任务处理完成")
print("开始生成压缩包")
zipCompress("./Nages自动翻译资源包")
print("压缩包生成完毕")

# 清理缓存文件
for i in tempFileDirList:
    deleteDir(i)
deleteDir("Nages自动翻译资源包")
