import os
import re
import json
import sys

def zhengli(input_file,output_file):
    """
    input_file:      输入：需整理的文本
    output_file：    输出：整理后的json文件
    整理的文本内每一行必须为key:value
    """
    dividing_line = "----------------------------------------------------------------------------------------------------"
    # 第一次处理 替换中文符号为英文符号 ，删除转义符号
    try:
        with open(input_file, 'r', encoding='utf-8') as f:
            text = f.read()
        # 替换中文符号为英文符号
        text = text.replace('：', ':').replace('“', '"').replace('”', '"').replace('，', ',').replace('\\', '').replace(
            '\\\\', '')
        # 删除可能出现的几行到几行的提示
        text = re.sub('//已完成 \d+ - \d+ 行-*\n', '', text)
        with open('处理后的文本.txt', 'w', encoding='utf-8') as f:
            f.write(text)
    except:
        a = input(f"{dividing_line}\n-没有找到【input_file】,请确认此文本存在于工具同一路径！\n-回车退出程序！")
        sys.exit(0)

    # 第二次处理 转换value里的双引号，逗号，冒号等等为中文符号
    with open("处理后的文本.txt", "r", encoding="utf-8") as f:
        text = f.read()
    regex = r'"(.+)":"(.+)"'
    matches = re.findall(regex, text)
    for match in matches:
        key, value = match
        value = value.replace('"', '“').replace(':', '：').replace(',', '，').replace('\\:', '：').replace('\\,', '，').replace(
            '\\"', '“')
        text = text.replace(f'"{key}":"{match[1]}"', f'"{key}":"{value}"')
    with open('处理后的文本.txt', 'w', encoding="utf-8") as f:
        f.writelines(text)

    # 第三次次处理 转换value里的  双引号，逗号，冒号等等
    with open("处理后的文本.txt", "r", encoding="utf-8") as f:
        text = f.read()
    regex = r'"(.+)": "(.+)"'
    matches = re.findall(regex, text)
    for match in matches:
        key, value = match
        value = value.replace('"', '“').replace(':', '：').replace(',', '，').replace('\\:', '：').replace('\\,', '，').replace(
            '\\"', '“')
        text = text.replace(f'"{key}": "{match[1]}"', f'"{key}":"{value}"')
    with open('处理后的文本.txt', 'w', encoding="utf-8") as f:
        f.writelines(text)

    # 第四次处理  匹配key:value 内容是否正确，且在后面无逗号的话加上逗号
    with open('处理后的文本.txt', 'r', encoding='utf-8') as f:
        lines = f.readlines()
    with open('处理后的文本.txt', 'w', encoding='utf-8') as f:
        for line in lines:
            match = re.match(r'^\s*("[^"]+"):("[^"]*")(,)?\s*$', line)
            if match:
                # 应该为key
                key = match.group(1)
                # 应该为value
                value = match.group(2)
                # 应该为逗号
                has_comma = bool(match.group(3))

                # 对于符合要求但行末没有逗号的行，在写入文件之前可以先添加上逗号
                if not has_comma:
                    line = line.strip() + ","

                # 将符合要求的行写入输出文件
                f.write(line)


    # 第五次处理 前后添加{}，最后一行去掉那个逗号
    with open('处理后的文本.txt', 'r', encoding='utf-8') as f:
        lines = f.readlines()
    os.remove('处理后的文本.txt')
    lines[0] = '{\n' + lines[0]
    lines[-1] = lines[-1].rstrip(',\n') + '\n'
    lines[-1] = lines[-1].rstrip() + '\n}'
    with open(output_file, 'w', encoding='utf-8') as f:
        f.write(''.join(lines))

    print(f"{dividing_line}\n-正在验证JSON文件格式是否正确...")

    try:
        with open(output_file, 'r', encoding="utf-8") as file:
            data = json.load(file)
        with open(output_file, 'w', encoding='utf-8') as f:
            json_str = json.dumps(data, ensure_ascii=False, indent=4)
            f.write(json_str)
        print(f"-JSON文件'{output_file}'的格式是正确的")
        a = input(f"-处理完成！已生成【{output_file}】\n{dividing_line}\n-回车退出程序！")
        sys.exit(0)
    except Exception as e:
        print(f"-JSON文件'{output_file}'的格式是错误的：{e}")
        a = input(f"-请根据异常查看，手动修改'{output_file}'\n{dividing_line}\n-回车退出程序！")
        sys.exit(0)

if __name__ == '__main__':
    # 整理的文本内每一行必须为key:value
    zhengli('需整理的文本.txt', '整理后的json文件.json')